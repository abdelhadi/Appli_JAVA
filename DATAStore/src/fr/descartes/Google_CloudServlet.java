package fr.descartes;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;

@SuppressWarnings("serial")
public class Google_CloudServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity employee = new Entity("Employee");
		employee.setProperty("firstName", "Antonio");
		employee.setProperty("lastName", "Salieri");
		employee.setProperty("hireDate", new Date());
		employee.setProperty("attendedHrTraining", true);
		datastore.put(employee);
		
		String firstName ="Antonio";
		Filter propertyFilter= new FilterPredicate("firstName",
				FilterOperator.EQUAL, firstName);
				Query q = new Query("Employee").setFilter(propertyFilter);
				List<Entity> results = datastore.prepare(q.setKeysOnly()).
				asList(FetchOptions.Builder.withDefaults());
				Query q1 = new Query("Person").addSort("lastName",SortDirection.ASCENDING);
				
				System.out.println(results.toString());
	}

}
